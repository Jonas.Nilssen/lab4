package datastructure;

import java.awt.Color;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int columnsProp;
    private int rowsProp;
    private CellState cellstateStruct[][];// = new int[columnsProp][rowsProp];
    //public int counter;
    

    public CellGrid(int rows, int columns, CellState initialState) {
		this.columnsProp = columns;
        this.rowsProp = rows;
        this.cellstateStruct = new CellState [rowsProp][columnsProp];
        for (int i = 0;i<this.rowsProp;i++){ //row
            for (int j = 0; j<this.columnsProp; j++){ //column
                this.cellstateStruct[i][j] = initialState;

            }
        }

	}

    @Override
    public int numRows() {
        return rowsProp;
    }

    @Override
    public int numColumns() {
        return columnsProp;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.rowsProp = row;
        this.columnsProp = column;
        this.cellstateStruct[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        return cellstateStruct[row][column];
    }

    @Override
    public IGrid copy() {

        IGrid cellCopyRef = new CellGrid(this.rowsProp, this.columnsProp, CellState.DEAD);
        for (int i = 0;i<this.rowsProp;i++){ //row
            for (int j = 0; j<this.columnsProp; j++){ //column
                cellCopyRef.set (i,j,this.get(i,j));
            }
        }
        return cellCopyRef;
    }
    
}
